const csvToJson = require('csvtojson');
const path = require('path');
const matchesPath = path.resolve(__dirname, '../data/matches.csv');
const deliveriesPath = path.resolve(__dirname, '../data/deliveries.csv');

function strikeRateOfBatsmen() {
    let matchId;
    csvToJson()
        .fromFile(matchesPath)
        .then((matchData) => {
            matchId = matchData.map((item) => {
                return {
                    id: item.id,
                    season: item.season
                };
            });
        });

    const strikeRateData = csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveryData) => {
            const mergedDataset = deliveryData.map((item) => {
                const yearObject = matchId.find(
                    (match) => match.id == String(item.match_id)
                );
                return {
                    ...item,
                    Year: yearObject.season
                };
            });

            const batsmenData = mergedDataset.reduce((acc, event) => {
                const batsman = event.batsman;
                const runs = Number(event.batsman_runs);
                const season = event.Year;

                if (acc[batsman] === undefined) {
                    acc[batsman] = {};
                }
                if (acc[batsman][season] === undefined) {
                    acc[batsman][season] = {
                        runs_scored: runs,
                        balls_faced: 1,
                        strike_rate: undefined,
                    }
                } else {
                    if (acc[batsman][season].strike_rate === undefined) {
                        acc[batsman][season].strike_rate = (acc[batsman][season].runs_scored / acc[batsman][season].balls_faced) * 100;
                    }
                    acc[batsman][season].runs_scored += runs;
                    acc[batsman][season].balls_faced++;
                    acc[batsman][season].strike_rate = (acc[batsman][season].runs_scored / acc[batsman][season].balls_faced) * 100;
                }

                return acc;
            }, {});
            return batsmenData;
        }).catch((error) => {
            throw new Error(error.message);
        });
    return strikeRateData;
}

module.exports = strikeRateOfBatsmen;