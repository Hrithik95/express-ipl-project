const csvToJson = require('csvtojson');
const path = require('path');
const matchesPath = path.resolve(__dirname, '../data/matches.csv');
const deliveriesPath = path.resolve(__dirname, '../data/deliveries.csv');

function topTenEcoBowlers() {
    let matchesDataId = [];
    csvToJson()
        .fromFile(matchesPath)
        .then((matchesData) => {
            matchesDataId = matchesData.filter((item, index, arr) => {
                return item.season === '2015';
            })
            .map((match)=>{
                return match.id;
            });
        });
    let economicalBowler = csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveriesData) => {
            let dataArr = deliveriesData.filter((item) => {
                return matchesDataId.includes(item.match_id);
            });
            let bowelerData = dataArr.reduce((acc, entry) => {
                if (acc[entry.bowler] === undefined) {
                    acc[entry.bowler] = {
                        runs: 0,
                        ball: 0,
                        eco: 0
                    }
                }
                acc[entry.bowler].runs += Number(entry.total_runs);
                acc[entry.bowler].ball += 1;
                let economy = (acc[entry.bowler].runs / acc[entry.bowler].ball) * 6;
                acc[entry.bowler].eco = economy;
                return acc;

            }, {});
            let answer = Object.entries(bowelerData);
            let sortedAccToEco = answer.sort((prev, curr) => {
                if (prev[1].eco > curr[1].eco) {
                    return 1;
                }
                else if (prev[1].eco < curr[1].eco) {
                    return -1;
                }
                else {
                    return 0;
                }
            });
            let top10EcoBowler = sortedAccToEco.slice(0, 10);
            return top10EcoBowler;
        }).catch((error) => {
            throw new Error(error.message);
        });

    return economicalBowler;
}

module.exports = topTenEcoBowlers;
