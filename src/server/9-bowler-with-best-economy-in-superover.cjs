const csvToJson = require('csvtojson');
const path = require('path');
const deliveriesPath = path.resolve(__dirname, '../data/deliveries.csv');

function bestEconomyOfBowlerInSuperOver() {
    let bestSuperOverEcoData = csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveries) => {
            let superOverEconomy = deliveries.reduce((acc, event) => {
                if (event.is_super_over != 0) {
                    if (acc[event.bowler] === undefined) {
                        acc[event.bowler] = {
                            runs: Number(event.total_runs),
                            balls: 1,
                            eco: 0
                        }
                    } else {
                        acc[event.bowler].runs += Number(event.total_runs);
                        acc[event.bowler].balls += 1;
                        let economy = (acc[event.bowler].runs / acc[event.bowler].balls) * 6;
                        acc[event.bowler].eco = economy;
                    }
                }
                return acc;
            }, {});
            let answer = Object.entries(superOverEconomy);
            let superOverEconomyArr = answer.sort((prev, curr) => {
                if (prev[1].eco > curr[1].eco) {
                    return 1;
                }
                else if (prev[1].eco < curr[1].eco) {
                    return -1;
                }
                else {
                    return 0;
                }
            });
            let bestBowler = superOverEconomyArr.slice(0, 1);
            return bestBowler;
        }).catch((error) => {
            throw new Error(error.message);
        });
    return bestSuperOverEcoData;
}

module.exports = bestEconomyOfBowlerInSuperOver;