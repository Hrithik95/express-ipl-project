const csvToJson = require('csvtojson');
const path = require('path');
const matchesPath = path.resolve(__dirname, '../data/matches.csv');

function tossAndMatchBothWon() {
    const tossAndMatchBothWonData = csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            let answer = data.reduce((acc, entry) => {
                if (acc[entry.toss_winner] === undefined) {
                    acc[entry.toss_winner] = {};
                }
                if (acc[entry.toss_winner] && entry.toss_winner === entry.winner) {
                    if (acc[entry.toss_winner]["toss&matchWon"] === undefined) {
                        acc[entry.toss_winner]["toss&matchWon"] = 1;
                    } else {
                        acc[entry.toss_winner]["toss&matchWon"] += 1;
                    }
                }
                return acc;

            }, {});
            return answer;
        }).catch((error) => {
            throw new Error(error.message);
        });
    return tossAndMatchBothWonData;
}

module.exports = tossAndMatchBothWon;