const csvToJson = require('csvtojson');
const path = require('path');
const deliveriesPath = path.resolve(__dirname, '../data/deliveries.csv');

function highestNumberOfTimesOnePlayerDismissedByAnother() {
    let dismissalData = csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveries) => {
            const batsmenDismissalData = deliveries.reduce((acc, event) => {
                if (event.player_dismissed !== '') {
                    if (!acc[event.batsman]) {
                        acc[event.batsman] = {};
                    }
                    if (!acc[event.batsman][event.bowler]) {
                        acc[event.batsman][event.bowler] = 0;
                    }
                    acc[event.batsman][event.bowler] += 1;
                    return acc;
                }

                return acc;
            }, {});

            const batsmenDismissalDataArray = Object.entries(batsmenDismissalData);
            let batsmenDismissalDataArrayResult = batsmenDismissalDataArray.reduce((acc, current, index, arr) => {
                let dataToBeSorted = Object.entries(current[1]);
                let sortedData = dataToBeSorted.sort((prev, curr) => {
                    //[1] contains the bowler name and no. of times he dismissed a particular batsman
                    if (prev[1] < curr[1]) {
                        return 1;
                    } else if (prev[1] > curr[1]) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
                let bowlerWithMostDismissal = sortedData.slice(0, 1);
                acc.push({
                    batsman: current[0],
                    dismissedMostTimesBy: bowlerWithMostDismissal[0]
                });
                return acc;
            },[]);
            return batsmenDismissalDataArrayResult;
        }).catch((error) => {
            throw new Error(error.message);
        });
    return dismissalData;
}

module.exports = highestNumberOfTimesOnePlayerDismissedByAnother;
