const { match } = require('assert');
const csvToJson = require('csvtojson');
const path = require('path');
const matchesPath = path.resolve(__dirname, '../data/matches.csv');
const deliveriesPath = path.resolve(__dirname, '../data/deliveries.csv');

function extraRunsConcededPerTeam() {
    let matchesDataId = [];
    csvToJson()
        .fromFile(matchesPath)
        .then((matchesData) => {
            matchesDataId = matchesData.filter((item, index, arr) => {
                return item.season === "2016";
            })
                .map((match) => {
                    return match.id;
                });

        });

    let data = csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveriesData) => {
            let extraRunPerTeam = deliveriesData.reduce((acc, curr) => {
                if (matchesDataId.includes(curr.match_id)) {
                    if (curr.bowling_team in acc) {
                        acc[curr.bowling_team] += Number(curr.extra_runs);
                    } else {
                        acc[curr.bowling_team] = Number(curr.extra_runs);
                    }
                }
                return acc;
            }, {});
            return extraRunPerTeam;

        }).catch((error) => {
            throw new Error(error.message);
        });
    return data;

}

module.exports = extraRunsConcededPerTeam;
