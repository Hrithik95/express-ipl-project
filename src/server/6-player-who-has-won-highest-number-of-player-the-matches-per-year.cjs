const csvToJson = require('csvtojson');
const path = require('path');
const matchesPath = path.resolve(__dirname, '../data/matches.csv');

function highestPlayerofMatchesPerSeason() {
    const highestPlayerOfMatchData = csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            let dataOfPlayerOfMatch = data.reduce((acc, entry) => {

                if (acc[entry.season] === undefined) {
                    acc[entry.season] = {};
                }
                if (acc[entry.season]) {
                    let playerOfMatch = entry["player_of_match"];
                    if (acc[entry.season][playerOfMatch] === undefined) {
                        acc[entry.season][playerOfMatch] = 1;
                    } else {
                        acc[entry.season][playerOfMatch] += 1;
                    }
                }
                return acc;
            }, {});
            let playerOfTheMatchArray = Object.entries(dataOfPlayerOfMatch);
            let playerOfTheMatchArrayResult = playerOfTheMatchArray.reduce((acc, current, index, arr) => {
                let dataToBeSorted = Object.entries(current[1]);
                let sortedData = dataToBeSorted.sort((prev, curr) => {
                    if (prev[1] < curr[1]) {
                        return 1;
                    } else if (prev[1] > curr[1]) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
                let highestMOM = sortedData.slice(0, 1);
                acc.push({
                    year: current[0],
                    highestPlayerOfMatch: highestMOM[0]
                });
                return acc;
            }, []);
            return playerOfTheMatchArrayResult;
        }).catch((error) => {
            throw new Error(error.message);
        });
    return highestPlayerOfMatchData;
}

module.exports = highestPlayerofMatchesPerSeason;