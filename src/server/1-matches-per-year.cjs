const csvToJson = require('csvtojson');
const fs = require('fs');
const path = require('path');
const matchesPath = path.resolve(__dirname, '../data/matches.csv');

function matchesPerYear() {
    const data = csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            let matchesPerYearData = data.reduce((acc,item) => {
                if (acc[item.season] === undefined) {
                    acc[item.season] = 1;
                }
                else {
                    acc[item.season] = (acc[item.season]) + 1;
                }
                return acc;
            },{});
            return matchesPerYearData;
        }).catch((error) => {
            throw new Error(error.message);
        });
    return data;
}

module.exports = matchesPerYear;