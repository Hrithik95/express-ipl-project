const express = require('express');
const app = express();

const matchesPerYear = require('./src/server/1-matches-per-year.cjs');
const matchesWonPerTeamPerYear = require('./src/server/2-matches-won-per-team-per-year.cjs');
const extraRunsConcededPerTeam = require('./src/server/3-extra-runs-conceded-per-team-2016.cjs');
const topTenEcoBowlers = require('./src/server/4-top-10-economical-bowler-2015.cjs');
const tossAndMatchBothWon = require('./src/server/5-number-of-times-each-team-won-the-toss-and-won-the-match.cjs');
const highestPlayerofMatchesPerSeason = require('./src/server/6-player-who-has-won-highest-number-of-player-the-matches-per-year.cjs');
const strikeRateOfBatsmen = require('./src/server/7-strike-rate-of-a-batsman-each-season.cjs');
const highestNumberOfTimesOnePlayerDismissedByAnother = require('./src/server/8-highest-number-of-times-one-player-has-been-dismissed-by-another.cjs');
const bestEconomyOfBowlerInSuperOver = require('./src/server/9-bowler-with-best-economy-in-superover.cjs');

app.get('/matches-per-year', (request, response) => {
    matchesPerYear()
        .then((data) => {
            response.json(data);
            return;
        }).catch((error) => {
            response.status(500).send(`<h1>Internal Server Error</h1>`);
        });
});

app.get('/matches-won-per-team-per-year', (request, response) => {
    matchesWonPerTeamPerYear()
        .then((data) => {
            response.json(data);
            return;
        }).catch((error) => {
            response.status(500).send(`<h1>Internal Server Error</h1>`);
        });
});

app.get('/extra-runs-conceded-per-team-2016', (request, response) => {
    extraRunsConcededPerTeam()
        .then((data) => {
            response.json(data);
            return;
        }).catch((error) => {
            response.status(500).send(`<h1>Internal Server Error</h1>`);
        });
});

app.get('/top-10-economical-bowler', (request, response) => {
    topTenEcoBowlers()
        .then((data) => {
            response.send(data);
        }).catch((error) => {
            response.status(500).send(`<h1>Internal Server Error</h1>`);
        });
});

app.get('/toss-and-match-both-won', (request, response) => {
    tossAndMatchBothWon()
        .then((data) => {
            response.json(data);
        }).catch((error) => {
            response.status(500).send(`<h1>Internal Server Error</h1>`);
        });
});

app.get('/highest-player-of-matches-award', (request, response) => {
    highestPlayerofMatchesPerSeason()
        .then((data) => {
            response.json(data);
        }).catch((error) => {
            response.status(500).send(`<h1>Internal Server Error</h1>`);
        });
});

app.get('/strike-rate-of-batsmen', (request, response) => {
    strikeRateOfBatsmen()
        .then((data) => {
            response.json(data);
        }).catch((error) => {
            response.status(500).send(`<h1>Internal Server Error</h1>`);
        });
});

app.get('/highest-dismissal-data', (request, response) => {
    highestNumberOfTimesOnePlayerDismissedByAnother()
        .then((data) => {
            response.send(data);
        }).catch((error) => {
            response.status(500).send(`<h1>Internal Server Error</h1>`);
        });
});

app.get('/best-superover-economy', (request, response) => {
    bestEconomyOfBowlerInSuperOver()
        .then((data) => {
            response.send(data);
        }).catch((error) => {
            response.status(500).send(`<h1>Internal Server Error</h1>`);
        });
});

app.use((request, response) => {
    response.status(404).send("Server could not found the requested web-page");
});

const port = process.env.PORT || 8000;

app.listen(port, (error) => {
    if (error) {
        console.error(error);
    } else {
        console.log("Server is up and running successfully on port:", port);
    }
});